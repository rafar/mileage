import MileageLayout from './components/mileage/MileageLayout';
const SettingsLayout = () => import('./components/settings/SettingsLayout');
import CostsLayout from './components/costs/CostsLayout';
import HomeLayout from './components/homepage/HomeLayout';
import YearPrintable from './components/mileage/YearPrintable';

export const routes = [
    {path: '/', component: HomeLayout, name: 'home'},
    {path: '/mileage', component: MileageLayout, name: 'mileage'},
    {path: '/printable', component: YearPrintable, name: 'printable'},
    {path: '/settings', component: SettingsLayout, name: 'settings'},
    {path: '/costs', component: CostsLayout, name: 'costs'},
    {path: '/autocomplete', component: SettingsLayout, name: 'autocomplete', children: [
            {path: '/destinations', component: SettingsLayout, name: 'autocomplete/destinations'},
            {path: '/descriptions', component: SettingsLayout, name: 'autocomplete/descriptions'},
        ]},
];