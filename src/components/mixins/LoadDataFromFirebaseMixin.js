import firebase from 'firebase'
import helpers from "../../assets/js/helpers";

export const LoadDataFromFirebaseMixin = {
    methods: {
        loadDataFromFirebase() {
            let vue = this;
            firebase.auth().onAuthStateChanged(user => {
                if (user) {
                    let userId = user.uid;
                    return firebase.database().ref('state/' + userId).once('value').then(function (snapshot) {
                        let stateFromFirebase = snapshot.val();
                        if (stateFromFirebase) {
                            vue.$store.replaceState(stateFromFirebase);
                            helpers.alert('Dane odczytane pomyślnie.');
                        }
                    });
                }
            });
        }
    },
};