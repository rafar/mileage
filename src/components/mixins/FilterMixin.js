export const FilterMixin = {
        filters: {
            toTwoDigit: (value) => {
                return ("0" + value).slice(-2);
            },
            roundToTwoDigits: (number, fixed = true) => {
                let rounded = Math.round(number * 100) / 100;
                if (fixed) {
                    rounded = rounded.toFixed(2);
                }
                return rounded;
            }
        }
    }
;