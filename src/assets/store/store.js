import Vue from 'vue'
import Vuex from 'vuex'
import common from './modules/common/common';
import mileage from './modules/mileage/mileage';
import settings from './modules/settings/settings'
import costs from './modules/costs/costs';
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex);

export const store = new Vuex.Store({
    plugins: [createPersistedState()],
    modules: {common, mileage, settings, costs},
    state: {},
    getters: {},
    mutations: {},
    actions: {},
    namespaced: true
});