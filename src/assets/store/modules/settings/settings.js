import Vue from 'vue';

const state = {
    user: {
        name: '',
        address: '',
        registration: '',
        engineCapacity: ''
    }
};

const getters = {
    getUser: state => {
        return state.user
    },
};

const mutations = {
    updateUserAttribute: (state, payload) => {
        for (let attribute in payload) {
            let value = payload[attribute];
            Vue.set(state.user, attribute, value);
        }
    },
    setUser: (state, user) => {
        state.user = user;
    },
};

const actions = {
    setUser(context) {
        context.commit('setUser')
    }
};

export default {
    state, mutations, actions, getters
}