import Vue from 'vue';

const state = {
    costs: {},
};

const getters = {
    costs: (state, getters) => {
        return state.costs;
    }
};

const mutations = {
    addCost: (state, payload) => {
        if (typeof(state.costs[payload.year]) === 'undefined') {
            Vue.set(state.costs, payload.year, {});
        }
        if (typeof(state.costs[payload.year][payload.month]) === 'undefined') {
            Vue.set(state.costs[payload.year], payload.month, []);
        }
        let index = state.costs[payload.year][payload.month].length;
        payload.cost.id = index + 1;
        Vue.set(state.costs[payload.year][payload.month], index, payload.cost);
    },
    deleteCost: (state, payload) => {
        Vue.delete(state.costs[payload.year][payload.month], payload.costId - 1);
    },
    reconstructCostsIds: (state, payload) => {
        for (let costIndex in state.costs[payload.year][payload.month]) {
            let cost = state.costs[payload.year][payload.month][costIndex];
            cost.id = parseInt(costIndex) + 1;
            Vue.set(state.costs[payload.year][payload.month], costIndex, cost);
        }
    }
};

const actions = {
    addCost(context, payload) {
        let year = context.getters.year;
        let month = context.getters.month;
        context.commit('addCost', {cost: payload, year: year, month: month})
    },
    deleteCost(context, payload) {
        let year = context.getters.year;
        let month = context.getters.month;
        context.commit('deleteCost', {costId: payload, year: year, month: month})
    },
    reconstructCostsIds(context) {
        let year = context.getters.year;
        let month = context.getters.month;
        context.commit('reconstructCostsIds', {year: year, month: month})
    }
};
export default {
    state, mutations, actions, getters
}