const state = {
    year: 0,
    month: 0
};

const getters = {
    year: state => {
        return state.year
    },
    month: state => {
        return state.month
    }
};

const mutations = {
    setCurrentDate: (state) => {
        let date = new Date();
        state.year = date.getFullYear();
        state.month = date.getMonth()+1;
    },
    yearChange: (state, payload) => {
        state.year += payload;
    },
    monthChange: (state, payload) => {
        state.month += payload;
        if (state.month === 13) {
            state.year++;
            state.month = 1;
        } else if (state.month === 0) {
            state.year--;
            state.month = 12;
        }
    },
};

const actions = {
    setCurrentDate (context) {
        context.commit('setCurrentDate')
    }
};

export default {
    state, mutations, actions, getters
}