import Vue from 'vue';

const state = {
    rides: {},
};

const getters = {
    rides: (state) => {
        return state.rides;
    },
    ridesExists: (state) => {
        for (let prop in state.rides) {
            if (state.rides.hasOwnProperty(prop))
                return true;
        }
        return false;
    }
};

const mutations = {
    addRide: (state, payload) => {
        if (typeof(state.rides[payload.year]) === 'undefined') {
            Vue.set(state.rides, payload.year, {});
        }
        if (typeof(state.rides[payload.year][payload.month]) === 'undefined') {
            Vue.set(state.rides[payload.year], payload.month, []);
        }
        let index = state.rides[payload.year][payload.month].length;
        payload.ride.id = index + 1;
        Vue.set(state.rides[payload.year][payload.month], index, payload.ride);
    },
    deleteRide: (state, payload) => {
        Vue.delete(state.rides[payload.year][payload.month], payload.rideId - 1);
    },
    reconstructIds: (state, payload) => {
        for (let rideIndex in state.rides[payload.year][payload.month]) {
            let ride = state.rides[payload.year][payload.month][rideIndex];
            ride.id = parseInt(rideIndex) + 1;
            Vue.set(state.rides[payload.year][payload.month], rideIndex, ride);
        }
    },
    sortByDate: (state, payload) => {
        let data = state.rides[payload.year][payload.month];

        function compare(a, b) {
            return a.date < b.date ? -1 : 1;
        }

        data.sort(compare);
        Vue.set(state.rides[payload.year], payload.month, data);
    }
};

const actions = {
    addRide(context, payload) {
        let year = context.getters.year;
        let month = context.getters.month;
        context.commit('addRide', {ride: payload, year: year, month: month})
    },
    deleteRide(context, payload) {
        let year = context.getters.year;
        let month = context.getters.month;
        context.commit('deleteRide', {rideId: payload, year: year, month: month})
    },
    reconstructIds(context) {
        let year = context.getters.year;
        let month = context.getters.month;
        context.commit('reconstructIds', {year: year, month: month})
    },
    sortByDate(context) {
        let year = context.getters.year;
        let month = context.getters.month;
        context.commit('sortByDate', {year: year, month: month});
        this.dispatch('reconstructIds');
    }
};
export default {
    state, mutations, actions, getters
}