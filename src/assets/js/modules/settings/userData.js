export function userData() {
    /** @type {string} */
    this.name = '';
    /** @type {string} */
    this.address = '';
    /** @type {string} */
    this.registration = '';
    /** @type {number} */
    this.capacity = 0;
}