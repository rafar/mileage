const CostsCalculations = {};

CostsCalculations.costsInMonth = function (year, month, stateCosts) {
    if (!stateCosts) {
        return [];
    }
    if (typeof(stateCosts[year]) === 'undefined') {
        return [];
    }
    if (typeof(stateCosts[year][month]) === 'undefined') {
        return [];
    }
    return stateCosts[year][month];
};

CostsCalculations.sumInMonth = function (year, month, stateCosts) {
    let sum = 0;
    let costsInMonth = CostsCalculations.costsInMonth(year, month, stateCosts);
    for (let index in costsInMonth) {
        let cost = costsInMonth[index];
        sum += Math.trunc(cost.value);
    }
    return sum;
};

CostsCalculations.costsInYear = function (year, month, stateCosts) {
    if (!stateCosts) {
        return [];
    }
    if (typeof(stateCosts[year]) === 'undefined') {
        return [];
    }
    return stateCosts[year];
};

CostsCalculations.sumInYear = function (year, month, stateCosts) {
    let sum = 0;
    let costsInYear = CostsCalculations.costsInYear(year, month, stateCosts);
    for (let monthIndex in costsInYear) {
        let monthCosts = costsInYear[monthIndex];
        for (let index in monthCosts) {
            if (monthIndex > month) {
                return sum;
            }
            let cost = monthCosts[index];
            sum += Math.trunc(cost.value);
        }
    }
    return sum;
};

CostsCalculations.costsMovedFromPreviousMonths = function (year, month, stateCosts) {
    let sum = 0;
    let costsInYear = CostsCalculations.costsInYear(year, month, stateCosts);
    for (let monthIndex in costsInYear) {
        let monthCosts = costsInYear[monthIndex];
        for (let index in monthCosts) {
            if (monthIndex >= month) {
                return sum;
            }
            let cost = monthCosts[index];
            sum += Math.trunc(cost.value);
        }
    }
    return sum;
};


export default CostsCalculations;