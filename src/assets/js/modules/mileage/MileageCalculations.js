const MileageCalculations = {};

MileageCalculations.ridesInMonth = function (year, month, stateRides) {
    if (!stateRides) {
        return [];
    }
    if (typeof(stateRides[year]) === 'undefined') {
        return [];
    }
    if (typeof(stateRides[year][month]) === 'undefined') {
        return [];
    }
    return stateRides[year][month];
};

MileageCalculations.ridesInYear = function (year, stateRides) {
    if (!stateRides) {
        return [];
    }
    if (typeof(stateRides[year]) === 'undefined') {
        return [];
    }
    return stateRides[year];
};

MileageCalculations.limitInMonth = function (year, month, stateRides) {
    let sum = 0;
    let ridesInMonth = MileageCalculations.ridesInMonth(year, month, stateRides);
    for (let index in ridesInMonth) {
        let ride = ridesInMonth[index];
        sum += ride.limit;
    }
    return sum;
};

MileageCalculations.limitInYear = function (year, month, stateRides) {
    let sum = 0;
    let ridesInYear = MileageCalculations.ridesInYear(year, stateRides);
    for (let monthIndex in ridesInYear) {
        let monthRides = ridesInYear[monthIndex];
        for (let index in monthRides) {
            if (monthIndex > month) {
                return sum;
            }
            let ride = monthRides[index];
            sum += ride.limit;
        }
    }
    return sum;
};

MileageCalculations.limitMovedFromPreviousMonths = function (year, month, stateRides) {
    let sum = 0;
    let ridesInYear = MileageCalculations.ridesInYear(year, stateRides);
    for (let monthIndex in ridesInYear) {
        let monthRides = ridesInYear[monthIndex];
        for (let index in monthRides) {
            if (monthIndex >= month) {
                return sum;
            }
            let ride = monthRides[index];
            sum += ride.limit;
        }
    }
    return sum;
};

export default MileageCalculations;