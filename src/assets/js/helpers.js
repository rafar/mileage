const helpers = {};
helpers.toTwoDigit = function (number) {
    return ("0" + number).slice(-2);
};

helpers.roundToTwoDigits = function (number, fixed = true) {
    let rounded = Math.round(number * 100) / 100;
    if (fixed) {
        rounded = rounded.toFixed(2);
    }
    return rounded;
};

helpers.alert = function (content, type = 'success') {
    let alert = $(".myAlert-top");
    alert.children('.content').html(content);
    alert.removeClass('alert-danger').removeClass('alert-success').addClass('alert-' + type);
    alert.show();
    setTimeout(function () {
        $(".myAlert-top").hide();
    }, 4000);
};

helpers.monthName = function (month) {
    switch (month) {
        case 1:
            return 'Styczeń';
        case 2:
            return 'Luty';
        case 3:
            return 'Marzec';
        case 4:
            return 'Kwiecień';
        case 5:
            return 'Maj';
        case 6:
            return 'Czerwiec';
        case 7:
            return 'Lipiec';
        case 8:
            return 'Sierpień';
        case 9:
            return 'Wrzesień';
        case 10:
            return 'Październik';
        case 11:
            return 'Listopad';
        case 12:
            return 'Grudzień';
        default:
            return month;
    }
};

export default helpers;