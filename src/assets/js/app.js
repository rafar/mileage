import Vue from 'vue'
import App from '../../App.vue'
import VueRouter from 'vue-router'
/*import Autocomplete from 'v-autocomplete'*/
import firebase from 'firebase'
import {store} from '../store/store'
import {routes} from '../../routes'

Vue.use(VueRouter);
/*Vue.use(Autocomplete);*/

let firebaseConfig = {
    apiKey: "AIzaSyCT9dGU3Is_g2P3SGsgn1kGYm-4enj_5h4",
    authDomain: "mileage-a9f92.firebaseapp.com",
    databaseURL: "https://mileage-a9f92.firebaseio.com",
    projectId: "mileage-a9f92",
    storageBucket: "",
    messagingSenderId: "27263051939"
};
firebase.initializeApp(firebaseConfig);

const router = new VueRouter({
    routes,
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        }
        if (to.hash) {
            return {selector: to.hash}
        }
    }
});

new Vue({
    el: '#app',
    store,
    router,
    render: h => h(App)
});
